package rkeeper_white_server_client

import (
	"encoding/json"
	"fmt"
	"net/url"
	"strconv"
	"testing"
	"time"
)

var serverURL = url.URL{
	Scheme: "https",
	Host:   "ws.ucs.ru",
	Path:   "/",
}

const token = "g9tRX8cFsS8=TRlajBoTiA5AqmKE8UHY7hrwX0agICiJUWIGfST+3tebi7tPyrtJu3W0SlxVoSkBK97Iobqeghja5eWM0v3VVAggeLxy0HCXYCnKMwsemNAKsaJ48VqSJ7bciI1Wfk7lfUnNQVkhxA2msi5GquGvIDPs8Ux1TLEZa2wQrAkc/6UhAJonsEkMhQcjPNZNkwDBd7i1HAmC+r6zcJW3uOhLSyzXVr2Lqfkk"

func TestWhiteServerApiImpl_GetMenu(t *testing.T) {
	api, err := New(WhiteServerConfig{
		ServerURL: &serverURL,
		Token:     token,
	})
	if err != nil {
		t.Logf("%v\n", err)
		t.FailNow()
	}
	menu, err := api.GetMenu(RequestParams{
		ObjectId: 481670007,
		Timeout:  0,
	})
	if err != nil {
		t.Logf("%v\n", err)
		t.FailNow()
	}
	t.Log(menu.TaskResponse.LastUpdatedAt.String())
}

func TestWhiteServerApiImpl_UpdateMenuAsync(t *testing.T) {
	api, err := New(WhiteServerConfig{
		ServerURL: &serverURL,
		Token:     token,
	})
	if err != nil {
		t.Logf("%v\n", err)
		t.FailNow()
	}
	task, err := api.UpdateMenuAsync(RequestParams{
		ObjectId: 481670007,
		Timeout:  0,
	})
	if err != nil {
		t.Logf("%v\n", err)
		t.FailNow()
	}
	t.Log(task.ResponseCommon.TaskGuid)
}

func TestWhiteServerApiImpl_AsyncOperations(t *testing.T) {
	api, err := New(WhiteServerConfig{
		ServerURL: &serverURL,
		Token:     token,
	})
	if err != nil {
		t.Logf("%v\n", err)
		t.FailNow()
	}
	//menu, err := api.GetMenu(RequestParams{
	//	ObjectId: 103960001,
	//	Timeout:  0,
	//})
	asyncResponse, err := api.GetStopListAsync(RequestParams{
		ObjectId: 481670007,
		Timeout:  0,
	})
	if err != nil {
		t.Logf("%v\n", err)
		t.FailNow()
	}
	time.Sleep(1 * time.Second)
	stopListResponse, err := api.GetTaskResponse(asyncResponse.ResponseCommon.TaskGuid)
	stopList := stopListResponse.(StopListResponse).TaskResponse.StopList
	if err != nil {
		t.Logf("%v\n", err)
		t.FailNow()
	}
	t.Log("Stop list dishes len: ", strconv.Itoa(len(stopList.Dishes)))
}

func TestWhiteServerApiImpl_CreateOrderAsync(t *testing.T) {
	api, err := New(WhiteServerConfig{
		ServerURL: &serverURL,
		Token:     token,
	})
	if err != nil {
		t.Logf("%v\n", err)
		t.FailNow()
	}
	params := RequestParams{
		ObjectId: 103960001,
		Timeout:  0,
	}
	originalOrderId := "4"
	order := CreateOrderPayload{
		OriginalOrderId: &originalOrderId,
		PaymentType:     PaymentType{Type: "card"},
		ExpeditionType:  pickup,
		Pickup:          &Pickup{Taker: Customer, ExpectedTime: ISO8601JSONTime{time.Now().Add(30 * time.Minute)}},
		Products: []Product{{
			Id:       "1001373",
			Name:     "Самса Алот",
			Price:    "449",
			Quantity: "1",
		}},
		Comment:         "Test samsa alot from Go 03.03",
		Price:           Price{Total: "449"},
		PersonsQuantity: 0,
	}
	orderTask, err := api.CreateOrderAsync(order, params)
	if err != nil {
		t.Logf("%v\n", err)
		t.FailNow()
	}
	var createdOrderResponse any
	for createdOrderResponse, err = api.GetTaskResponse(orderTask.ResponseCommon.TaskGuid); err != nil; {
		if wsError, ok := err.(*WsError); ok {
			if wsError.Code == "AwaitingAnswer" {
				time.Sleep(2 * time.Second)
				continue
			}
		}
		t.Logf("%v\n", err)
		t.FailNow()
	}
	createdOrder := createdOrderResponse.(*CreateOrderResponse).TaskResponse.Order
	t.Logf("%+v\n", createdOrder)
}

func TestWhiteServerApiImpl_GetTaskResponse(t *testing.T) {
	api, err := New(WhiteServerConfig{
		ServerURL: &serverURL,
		Token:     token,
	})
	if err != nil {
		t.Logf("%v\n", err)
		t.FailNow()
	}
	var createdOrderResponse any
	for createdOrderResponse, err = api.GetTaskResponse("bac6920b-8826-489b-89f1-4caae1d6705d"); err != nil; {
		t.Logf("%v\n", err)
		//t.FailNow()
		time.Sleep(2 * time.Second)
	}
	createdOrder := createdOrderResponse.(*CreateOrderResponse).TaskResponse.Order
	t.Logf("%+v\n", createdOrder)
}

func TestGetStopListProductsFromRkeeper(t *testing.T) {
	api, err := New(WhiteServerConfig{
		ServerURL: &serverURL,
		Token:     token,
	})
	stopListTaskGuid, err := api.GetStopListAsync(RequestParams{
		ObjectId: 481670007, //103960001,
		Timeout:  0,
	})
	if err != nil {
		fmt.Println(err)
		t.Fatalf("%+v\n", err)
	}
	idGuid := stopListTaskGuid.ResponseCommon.TaskGuid

	var LoadStopListResponse any
	for LoadStopListResponse, err = api.GetTaskResponse(idGuid); err != nil; LoadStopListResponse, err = api.GetTaskResponse(idGuid) {
		fmt.Printf("%v\n", err)
		if wsError, ok := err.(*WsError); ok {
			if wsError.Code == "AwaitingAnswer" {
				time.Sleep(2 * time.Second)
				continue
			}

		}
		t.Fatalf("%+v\n", err)
	}
	stopListResponse, ok := LoadStopListResponse.(*StopListResponse)
	if !ok {
		marshalled, err2 := json.Marshal(LoadStopListResponse)
		if err2 != nil {
			err2 = fmt.Errorf("got unknown object type instead of StopListResponse and cannot json.Marshall it")
			t.Fatalf("%+v\n", err2)
		}
		err2 = fmt.Errorf("got unknown object type instead of StopListResponse: %s", string(marshalled))
		t.Fatalf("%+v\n", err2)
	}
	StopListResponse := stopListResponse.TaskResponse.StopList

	var result []string
	for _, ch := range StopListResponse.Dishes {
		result = append(result, ch.Id)
	}
}

func TestJson(t *testing.T) {
	jsonString := `{"error":{"wsError":{"code":"AwaitingAnswer","desc":"Awaiting Answer"}}}`
	response := AsyncResponse{}
	err := json.Unmarshal([]byte(jsonString), &response)
	if err != nil {
		t.Fatalf("Cannot unmarshall response body for WhiteServer: %+v\n", err)
	}
	t.Logf("Error code: %+v\n", response.Error.WsError.Code)
}
