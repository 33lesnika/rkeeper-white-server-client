package rkeeper_white_server_client

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/http/httputil"
	"net/url"
)

type WhiteServerApi interface {
	GetMenu(params RequestParams) (*GetMenuResponse, error)
	UpdateMenuAsync(params RequestParams) (*WsResponse, error)
	GetOrderAsync(orderGuid string, params RequestParams) (*GetOrderResponse, error)
	CreateOrderAsync(order CreateOrderPayload, params RequestParams) (*CreateOrderResponse, error)
	GetStopListAsync(params RequestParams) (*WsResponse, error)
	GetTaskResponse(taskGuid string) (any, error)
	PayOrderAsync(payload PayOrderRequestPayload) (*WsResponse, error)
	CompleteOrder(params RequestParams, orderGuid string) (*CompleteOrderResponse, error)
	CancelOrderAsync(params RequestParams, orderGuid string) (*WsResponse, error)
}

type whiteServerApiImpl struct {
	config     WhiteServerConfig
	requestURL string
	headers    *http.Header
	debug      bool
}

type WhiteServerConfig struct {
	ServerURL  *url.URL
	Token      string
	HttpClient *http.Client
	Debug      bool
}

func New(config WhiteServerConfig) (WhiteServerApi, error) {
	if config.HttpClient == nil {
		config.HttpClient = http.DefaultClient
	}
	if !config.ServerURL.IsAbs() {
		err := fmt.Errorf("server URL is not absolute, given URL: %s\n", config.ServerURL.String())
		if config.Debug {
			fmt.Printf("%v", err)
		}
		return nil, err
	}
	//goland:noinspection SpellCheckingInspection
	apiURL, _ := url.Parse("wsserverlp/api/v2/aggregators/Create")
	return whiteServerApiImpl{
		config:     config,
		requestURL: config.ServerURL.ResolveReference(apiURL).String(),
		headers: &http.Header{
			"AggregatorAuthentication": {config.Token},
			"Content-Type":             {"application/json;charset=utf-8"},
		},
		debug: config.Debug,
	}, nil
}

func (ws whiteServerApiImpl) GetMenu(params RequestParams) (*GetMenuResponse, error) {
	request := GetMenuRequest{
		Request: Request{TaskType: GetMenu},
		Params: struct {
			Sync RequestParams `json:"sync"`
		}{params},
	}
	response := &GetMenuResponse{}
	_, err := ws.makeRequest(request, response)
	err = checkWsError("GetMenu", &response.WsResponse, err)
	if err != nil {
		return nil, err
	}
	return response, nil
}

func (ws whiteServerApiImpl) UpdateMenuAsync(params RequestParams) (*WsResponse, error) {
	request := UpdateMenuRequest{
		Request: Request{
			TaskType: UpdateMenu,
		},
		Params: struct {
			Async RequestParams `json:"async"`
		}{params},
	}
	response := &WsResponse{}
	_, err := ws.makeRequest(request, response)
	err = checkWsError("UpdateMenuAsync", response, err)
	if err != nil {
		return nil, err
	}
	return response, nil
}

func (ws whiteServerApiImpl) CreateOrderAsync(order CreateOrderPayload, params RequestParams) (*CreateOrderResponse, error) {
	request := CreateOrderRequest{
		Request: Request{TaskType: CreateOrder},
		Params: struct {
			Async RequestParams      `json:"async"`
			Order CreateOrderPayload `json:"order"`
		}{params, order},
	}
	response := &CreateOrderResponse{}
	_, err := ws.makeRequest(request, response)
	err = checkWsError("CreateOrderAsync", &response.WsResponse, err)
	if err != nil {
		return nil, err
	}
	return response, nil
}

func (ws whiteServerApiImpl) GetOrderAsync(orderGuid string, params RequestParams) (*GetOrderResponse, error) {
	request := GetOrderRequest{
		Request: Request{TaskType: GetOrder},
		Params: struct {
			Sync      RequestParams `json:"sync"`
			OrderGuid string        `json:"orderGuid"`
		}{params, orderGuid},
	}
	response := &GetOrderResponse{}
	_, err := ws.makeRequest(request, response)
	err = checkWsError("GetOrderAsync", &response.WsResponse, err)
	if err != nil {
		return nil, err
	}
	return response, nil
}

func (ws whiteServerApiImpl) GetTaskResponse(taskGuid string) (any, error) {
	request := GetTaskRequest{
		Request: Request{TaskType: GetTaskResponse},
		Params: struct {
			TaskGuid string `json:"taskGuid"`
		}{taskGuid},
	}
	response := &AsyncResponse{}
	bodyBytes, err := ws.makeRequest(request, response)
	if err != nil {
		fmt.Printf("Cannot read response body for WhiteServer, method: GetTaskResponse\n")
		return nil, err
	}
	if response.Error != nil {
		return nil, &response.Error.WsError
	}
	if response.WsError != nil {
		return nil, response.WsError
	}
	if response.ResponseCommon == nil {
		return nil, fmt.Errorf("no ResponseCommon in response from WhiteServer on GetTaskResponse method\n")
	}
	result, err := getTaskResponseType(response.ResponseCommon.TaskType)
	err = json.Unmarshal(bodyBytes, result)
	if err != nil {
		fmt.Printf("Cannot unmarshall taskResponse for WhiteServer, method: GetTaskResponse\n")
		return nil, err
	}
	return result, nil
}

var asyncResponseTypeMap = map[TaskType]func() any{
	GetOrder: func() any {
		return &GetOrderResponse{}
	},
	UpdateMenu: func() any {
		return &UpdateMenuResponse{}
	},
	CreateOrder: func() any {
		return &CreateOrderResponse{}
	},
	GetStopList: func() any {
		return &StopListResponse{}
	},
	PayOrder: func() any {
		return &PayOrderResponse{}
	},
	CancelOrder: func() any {
		return &CancelResponse{}
	},
}

func getTaskResponseType(taskType TaskType) (any, error) {
	if generate, found := asyncResponseTypeMap[taskType]; found {
		return generate(), nil
	} else {
		return nil, fmt.Errorf("unknown taskType: %s\n", taskType)
	}
}

func (ws whiteServerApiImpl) GetStopListAsync(params RequestParams) (*WsResponse, error) {
	request := GetStopListRequest{
		Request: Request{TaskType: GetStopList},
		Params: struct {
			Async RequestParams `json:"async"`
		}{params},
	}
	response := &WsResponse{}
	_, err := ws.makeRequest(request, response)
	err = checkWsError("GetStopListAsync", response, err)
	if err != nil {
		return nil, err
	}
	return response, nil
}

func (ws whiteServerApiImpl) PayOrderAsync(payload PayOrderRequestPayload) (*WsResponse, error) {
	request := PayOrderRequest{
		Request: Request{TaskType: PayOrder},
		Params:  payload,
	}
	response := &WsResponse{}
	_, err := ws.makeRequest(request, response)
	err = checkWsError("PayOrderAsync", response, err)
	if err != nil {
		return nil, err
	}
	return response, nil
}

func (ws whiteServerApiImpl) CompleteOrder(params RequestParams, orderGuid string) (*CompleteOrderResponse, error) {
	request := CompleteOrderRequest{
		Request: Request{TaskType: CompleteOrder},
		Params: struct {
			Sync      RequestParams `json:"sync"`
			OrderGuid string        `json:"orderGuid"`
		}{params, orderGuid},
	}
	response := &CompleteOrderResponse{}
	_, err := ws.makeRequest(request, response)
	err = checkWsError("CompleteOrder", &response.WsResponse, err)
	if err != nil {
		return nil, err
	}
	return response, nil
}

func (ws whiteServerApiImpl) CancelOrderAsync(params RequestParams, orderGuid string) (*WsResponse, error) {
	request := CancelOrderRequest{
		Request: Request{TaskType: CancelOrder},
		Params: struct {
			Async     RequestParams `json:"async"`
			OrderGuid string        `json:"orderGuid"`
		}{params, orderGuid},
	}
	response := &WsResponse{}
	_, err := ws.makeRequest(request, response)
	err = checkWsError("CompleteOrder", response, err)
	if err != nil {
		return nil, err
	}
	return response, nil
}

func (ws whiteServerApiImpl) makeRequest(request any, response any) ([]byte, error) {
	requestBody, _ := json.Marshal(request)
	httpRequest, err := http.NewRequest(http.MethodPost,
		ws.requestURL,
		bytes.NewReader(requestBody))
	if err != nil {
		fmt.Printf("Cannot create http request\n")
		return nil, err
	}
	httpRequest.Header = *ws.headers
	reqDump, err := httputil.DumpRequestOut(httpRequest, true)
	if err != nil {
		fmt.Printf("%v\n", err)
	}
	if ws.debug {
		fmt.Printf("REQUEST:\n%s\n", string(reqDump))
	}
	httpResponse, err := ws.config.HttpClient.Do(httpRequest)
	defer func(Body io.ReadCloser) {
		_ = Body.Close()
	}(httpResponse.Body)
	respDump, err := httputil.DumpResponse(httpResponse, true)
	if err != nil {
		fmt.Printf("%v\n", err)
	}
	if ws.debug {
		fmt.Printf("RESPONSE:\n%s\n", string(respDump))
	}
	defer func(Body io.ReadCloser) {
		_ = Body.Close()
	}(httpResponse.Body)
	bodyBytes, err := io.ReadAll(httpResponse.Body)
	err = json.Unmarshal(bodyBytes, response)
	if err != nil {
		fmt.Printf("Cannot unmarshall response body for WhiteServer\n")
		return nil, err
	}
	return bodyBytes, nil
}

func checkWsError(methodName string, response *WsResponse, err error) error {
	if err != nil {
		return err
	}
	if response.Error != nil {
		return fmt.Errorf("error from WhiteServer on %s method: %v\n", methodName, response.Error.WsError)
	}
	return nil
}
