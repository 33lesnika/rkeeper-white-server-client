package rkeeper_white_server_client

import (
	"errors"
	"fmt"
	"time"
)

type Request struct {
	TaskType TaskType `json:"taskType"`
}

type RequestParams struct {
	ObjectId int `json:"objectId"`
	Timeout  int `json:"timeout"`
}

type WsResponse struct {
	ResponseCommon *ResponseCommon `json:"responseCommon"`
	Error          *Error          `json:"error"`
}

type AsyncResponse struct {
	WsResponse
	WsError *WsError `json:"wsError"`
}

type ResponseCommon struct {
	TaskGuid string   `json:"taskGuid"`
	TaskType TaskType `json:"taskType"`
	ObjectId int      `json:"objectId"`
}
type TaskType string

const (
	GetMenu         TaskType = "GetMenu"
	UpdateMenu      TaskType = "UpdateMenu"
	CreateOrder     TaskType = "CreateOrder"
	GetOrder        TaskType = "GetOrder"
	GetStopList     TaskType = "GetStopList"
	PayOrder        TaskType = "PayOrder"
	CompleteOrder   TaskType = "CompleteOrder"
	CancelOrder     TaskType = "CancelOrder"
	GetTaskResponse TaskType = "GetTaskResponse"
)

type WsError struct {
	Code        string `json:"code"`
	Description string `json:"desc"`
}

func (e *WsError) Error() string {
	return fmt.Sprintf("rkeeper responded with wsError: code %s; desc: %s", e.Code, e.Description)
}

type Error struct {
	WsError WsError `json:"wsError"`
}

type GetMenuResponse struct {
	WsResponse
	TaskResponse *struct {
		LastUpdatedAt ISO8601JSONTime `json:"lastUpdatedAt"`
		Menu          struct {
			Categories []struct {
				Id       string `json:"id"`
				Name     string `json:"name"`
				ParentId string `json:"parentId,omitempty"`
			} `json:"categories"`
			Products []struct {
				Id          string   `json:"id"`
				CategoryId  string   `json:"categoryId"`
				Name        string   `json:"name"`
				Price       string   `json:"price"`
				SchemeId    string   `json:"schemeId,omitempty"`
				Description string   `json:"description"`
				ImageUrls   []string `json:"imageUrls"`
				Measure     struct {
					Value string `json:"value"`
					Unit  string `json:"unit"`
				} `json:"measure"`
			} `json:"products"`
			IngredientsSchemes []struct {
				Id                string `json:"id"`
				IngredientsGroups []struct {
					Id       string `json:"id"`
					MinCount int    `json:"minCount,omitempty"`
					MaxCount int    `json:"maxCount,omitempty"`
				} `json:"ingredientsGroups"`
			} `json:"ingredientsSchemes"`
			IngredientsGroups []struct {
				Id          string   `json:"id"`
				Name        string   `json:"name"`
				Ingredients []string `json:"ingredients"`
			} `json:"ingredientsGroups"`
			Ingredients []struct {
				Id          string   `json:"id"`
				Name        string   `json:"name"`
				Price       string   `json:"price"`
				Description string   `json:"description"`
				ImageUrls   []string `json:"imageUrls"`
				Measure     struct {
					Value string `json:"value"`
					Unit  string `json:"unit"`
				} `json:"measure"`
			} `json:"ingredients"`
		} `json:"menu"`
	} `json:"taskResponse"`
}

type UpdateMenuResponse struct {
	WsResponse
	TaskResponse *struct {
		Status string `json:"status"`
	} `json:"taskResponse"`
}

type GetOrderResponse struct {
	WsResponse
	TaskResponse *struct {
		OriginalOrderId string `json:"originalOrderId"`
		CreatedAt       string `json:"createdAt"`
		Status          struct {
			Value string `json:"value"`
		} `json:"status"`
		Delivery struct {
			ExpectedTime string `json:"expectedTime"`
		} `json:"delivery"`
		Products []struct {
			Ingredients []struct {
				Id       string `json:"id"`
				Name     string `json:"name"`
				Quantity int    `json:"quantity"`
				Price    string `json:"price"`
			} `json:"ingredients"`
			Id       string `json:"id"`
			Name     string `json:"name"`
			Price    string `json:"price"`
			Quantity int    `json:"quantity"`
		} `json:"products"`
		Comment string `json:"comment"`
		Price   struct {
			Total int `json:"total"`
		} `json:"price"`
		PersonsQuantity int `json:"personsQuantity"`
	} `json:"taskResponse"`
}

type CreateOrderResponse struct {
	WsResponse
	TaskResponse *struct {
		Order struct {
			OrderGuid *string `json:"orderGuid"`
			Status    *struct {
				Value         string `json:"value"`
				IsBillPrinted bool   `json:"isBillPrinted"`
			} `json:"status"`
			TableCode       int `json:"tableCode"`
			RejectingReason *struct {
				Code    string `json:"code"`
				Message string `json:"message"`
			} `json:"rejectingReason"`
		} `json:"order"`
	} `json:"taskResponse"`
}

type CustomerStruct struct {
	Name  string `json:"name"`
	Phone string `json:"phone"`
}

type PaymentType struct {
	Type string `json:"type"`
}

type ExpeditionType string

const (
	pickup   ExpeditionType = "pickup"
	delivery ExpeditionType = "delivery"
)

type Taker string

const (
	Courier  Taker = "courier"
	Customer Taker = "customer"
)

type Pickup struct {
	Courier      *CourierStruct  `json:"courier,omitempty"`
	ExpectedTime ISO8601JSONTime `json:"expectedTime"`
	Taker        Taker           `json:"taker"`
}

type ISO8601JSONTime struct {
	time.Time
}

//goland:noinspection GoMixedReceiverTypes
func (t ISO8601JSONTime) MarshalJSON() ([]byte, error) {
	//do your serializing here
	stamp := fmt.Sprintf("\"%s\"", t.Time.Format(time.RFC3339))
	return []byte(stamp), nil
}

//goland:noinspection GoMixedReceiverTypes
func (t *ISO8601JSONTime) UnmarshalJSON(data []byte) error {
	if data[0] != '"' {
		return errors.New("ISO8601JSONTime: UnmarshalJSON: cannot parse date: " + string(data))
	}
	parsed, err := time.Parse(time.RFC3339, string(data[1:len(data)-1]))
	if err != nil {
		return errors.New("ISO8601JSONTime: UnmarshalJSON: " + err.Error())
	}
	t.Time = parsed
	return nil
}

type CourierStruct struct {
	Name  string `json:"name"`
	Phone string `json:"phone"`
}

type Delivery struct {
	ExpectedTime ISO8601JSONTime `json:"expectedTime"`
	Address      Address         `json:"address"`
}

type Address struct {
	FullAddress string      `json:"fullAddress"`
	Subway      string      `json:"subway"`
	Region      string      `json:"region"`
	City        City        `json:"city"`
	Street      Street      `json:"street"`
	HouseNumber string      `json:"houseNumber"`
	FlatNumber  string      `json:"flatNumber"`
	Entrance    string      `json:"entrance"`
	Intercom    string      `json:"intercom"`
	Floor       string      `json:"floor"`
	Coordinates Coordinates `json:"coordinates"`
}

type City struct {
	Name string `json:"name"`
}

type Street struct {
	Name string `json:"name"`
}

type Coordinates struct {
	Latitude  string `json:"latitude"`
	Longitude string `json:"longitude"`
}

type Product struct {
	Id          string       `json:"id"`
	Name        string       `json:"name"`
	Price       string       `json:"price"`
	Quantity    string       `json:"quantity"`
	Ingredients []Ingredient `json:"ingredients,omitempty"`
}

type Ingredient struct {
	Id       string `json:"id"`
	Name     string `json:"name"`
	Quantity string `json:"quantity"`
	Price    string `json:"price"`
}

type Price struct {
	Total string `json:"total"`
}
type CreateOrderPayload struct {
	OriginalOrderId *string         `json:"originalOrderId,omitempty"`
	Customer        *CustomerStruct `json:"customer,omitempty"`
	PaymentType     PaymentType     `json:"payment"`
	ExpeditionType  ExpeditionType  `json:"expeditionType"`
	Pickup          *Pickup         `json:"pickup,omitempty"`
	Delivery        *Delivery       `json:"delivery,omitempty"`
	Products        []Product       `json:"products"`
	Comment         string          `json:"comment"`
	Price           Price           `json:"price"`
	PersonsQuantity int             `json:"personsQuantity"`
}

type StopListDish struct {
	Id   string `json:"id"`
	Name string `json:"name"`
	Type string `json:"type"`
}

type LimitedListDish struct {
	StopListDish
	Quantity int `json:"quantity"`
}

type StopListResponse struct {
	TaskResponse struct {
		StopList struct {
			Dishes []StopListDish `json:"dishes"`
		} `json:"stopList"`
		LimitedList struct {
			Dishes []interface{} `json:"dishes"`
		} `json:"limitedList"`
	} `json:"taskResponse"`
}

type Payment struct {
	Amount   int    `json:"amount"`
	Currency string `json:"currency"`
}

type GetMenuRequest struct {
	Request
	Params struct {
		Sync RequestParams `json:"sync"`
	} `json:"params"`
}

type UpdateMenuRequest struct {
	Request
	Params struct {
		Async RequestParams `json:"async"`
	} `json:"params"`
}

type GetStopListRequest struct {
	Request
	Params struct {
		Async RequestParams `json:"async"`
	} `json:"params"`
}

type CreateOrderRequest struct {
	Request
	Params struct {
		Async RequestParams      `json:"async"`
		Order CreateOrderPayload `json:"order"`
	} `json:"params"`
}

type GetTaskRequest struct {
	Request
	Params struct {
		TaskGuid string `json:"taskGuid"`
	} `json:"params"`
}

type GetOrderRequest struct {
	Request
	Params struct {
		Sync      RequestParams `json:"sync"`
		OrderGuid string        `json:"orderGuid"`
	} `json:"params"`
}

type PayOrderRequest struct {
	Request
	Params PayOrderRequestPayload `json:"params"`
}

type PayOrderRequestPayload struct {
	Async               RequestParams `json:"async"`
	OrderGuid           string        `json:"orderGuid"`
	IsFullOrderRequired bool          `json:"isFullOrderRequired"`
	Payments            []Payment     `json:"payments"`
}

type PayOrderResponse struct {
	WsResponse
	TaskResponse *struct {
		Order struct {
			OriginalOrderId string `json:"originalOrderId"`
			OrderGuid       string `json:"orderGuid"`
			CreatedAt       string `json:"createdAt"`
			Status          struct {
				Value         string `json:"value"`
				IsBillPrinted bool   `json:"isBillPrinted"`
			} `json:"status"`
			Products []struct {
				Ingredients []struct {
					Id       int    `json:"id"`
					Name     string `json:"name"`
					Quantity int    `json:"quantity"`
				} `json:"ingredients"`
				Id       int    `json:"id"`
				Name     string `json:"name"`
				Price    int    `json:"price"`
				Quantity int    `json:"quantity"`
			} `json:"products"`
			Comment         string `json:"comment"`
			PersonsQuantity int    `json:"personsQuantity"`
			Price           struct {
				Total     int `json:"total"`
				SummToPay int `json:"summToPay"`
			} `json:"price"`
			AppliedPayments []struct {
				Amount  int    `json:"amount"`
				Guid    string `json:"guid"`
				PayType string `json:"payType"`
			} `json:"appliedPayments"`
			WaiterId int `json:"waiterId"`
		} `json:"order"`
	} `json:"taskResponse"`
}

type CompleteOrderRequest struct {
	Request
	Params struct {
		Sync      RequestParams `json:"sync"`
		OrderGuid string        `json:"orderGuid"`
	} `json:"params"`
}

type CompleteOrderResponse struct {
	WsResponse
	TaskResponse *struct {
		Order struct {
			Price struct {
				Total int `json:"total"`
			} `json:"price"`
		} `json:"order"`
	} `json:"taskResponse"`
}

type CancelOrderRequest struct {
	Request
	Params struct {
		Async     RequestParams `json:"async"`
		OrderGuid string        `json:"orderGuid"`
	} `json:"params"`
}

type CancelResponse struct {
	WsResponse
	TaskResponse *struct {
		Status string `json:"status"`
	} `json:"taskResponse"`
}
