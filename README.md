# Simple Go client for RKeeper White Server API

Usage:
- Get menu:
```go
	serverURL := url.URL{
		Scheme: "https",
		Host:   "ws.ucs.ru",
		Path:   "/",
	}
	api, err := New(WhiteServerConfig{
		ServerURL: &serverURL,
		Token:     "API_TOKEN",
	})
	if err != nil {
		fmt.Printf("%v\n", err)
		os.Exit(-1)
	}
	menu, err := api.GetMenu(RequestParams{
		ObjectId: 1000000000,
		Timeout:  0,
	})
	if err != nil {
        fmt.Printf("%v\n", err)
        os.Exit(-1)
	}
```